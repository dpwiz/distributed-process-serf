module Control.Distributed.Process.Serf.Types
  ( Command(..)
  , CallbackHandler
  -- * Internals
  , Frame(..)
  , ReplyHeader(..)
  , headerFrame
  ) where

import           Control.Distributed.Process (Process)
import           Data.Binary                 (Binary (..))
import qualified Data.HashMap.Strict         as HM
import qualified Data.MessagePack            as MP
import           Data.Text                   (Text)

newtype Frame = Frame (HM.HashMap Text MP.Object)
  deriving (Show)

instance Binary Frame where
  put (Frame f) = put (MP.pack f)
  get = do
    bsl <- get
    case MP.unpack bsl of
      Nothing -> fail "Not a Serf frame."
      Just f -> pure (Frame f)

data ReplyHeader = ReplyHeader
  { replySeq   :: !Int
  , replyError :: !Text
  }

headerFrame :: Frame -> Maybe ReplyHeader
headerFrame (Frame f) = ReplyHeader
  <$> (MP.fromObject =<< HM.lookup "Seq" f)
  <*> (MP.fromObject =<< HM.lookup "Error" f)

type CallbackHandler = Either Text (Maybe Frame) -> Process ()

data Command = Command
  { cmdName      :: !Text
  , cmdWantsBody :: !Bool
  , cmdOneShot   :: !Bool
  , cmdBody      :: !(Maybe Frame)
  , cmdCallback  :: !CallbackHandler
  }
