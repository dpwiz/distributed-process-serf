module Control.Distributed.Process.Serf.Commands
  ( handshake
  , auth
  , event
  , forceLeave
  , join
  , members
  , membersFiltered
  , tags
  , stream
  , monitor
  , stop
  , leave
  , query
  , respond
  , installKey
  , useKey
  , removeKey
  , listKeys
  , stats
  , getCoordinate
  ) where

import qualified Data.ByteString.Char8 as BS
import qualified Data.HashMap.Strict   as HM
import qualified Data.MessagePack      as MP
import qualified Data.Text             as T

import Control.Distributed.Process.Serf.Types

handshake ::
     CallbackHandler
  -> Command
handshake cb = Command
  { cmdName      = "handshake"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Version" .= (1 :: Int)
    ]
  , cmdCallback  = cb
  }

auth ::
     T.Text
  -> CallbackHandler
  -> Command
auth key cb = Command
  { cmdName      = "auth"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "AuthKey" .= key
    ]
  , cmdCallback  = cb
  }

event ::
     T.Text
  -> BS.ByteString
  -> Bool
  -> CallbackHandler
  -> Command
event name payload coalesce cb = Command
  { cmdName      = "event"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Name"     .= name
    , "Payload"  .= payload
    , "Coalesce" .= coalesce
    ]
  , cmdCallback  = cb
  }

forceLeave ::
     T.Text
  -> CallbackHandler
  -> Command
forceLeave node cb = Command
  { cmdName      = "force-leave"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Node" .= node
    ]
  , cmdCallback  = cb
  }

join ::
     [T.Text]
  -> Bool
  -> CallbackHandler
  -> Command
join existing replay cb = Command
  { cmdName      = "join"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "AuthKey" .= existing
    , "Replay"  .= replay
    ]
  , cmdCallback  = cb
  }

members ::
     CallbackHandler
  -> Command
members cb = Command
  { cmdName      = "members"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Nothing
  , cmdCallback  = cb
  }

membersFiltered ::
     HM.HashMap T.Text T.Text
  -> T.Text
  -> T.Text
  -> CallbackHandler
  -> Command
membersFiltered ts status name cb = Command
  { cmdName      = "members-filtered"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Tags"   .= ts
    , "Status" .= status
    , "Name"   .= name
    ]
  , cmdCallback  = cb
  }

tags ::
     HM.HashMap T.Text T.Text
  -> [T.Text]
  -> CallbackHandler
  -> Command
tags add delete cb = Command
  { cmdName      = "tags"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Tags"       .= add
    , "DeleteTags" .= delete
    ]
  , cmdCallback  = cb
  }

stream ::
     T.Text
  -> CallbackHandler
  -> Command
stream type_ cb = Command
  { cmdName      = "stream"
  , cmdWantsBody = True
  , cmdOneShot   = False
  , cmdBody      = Just $ Frame
    [ "Type" .= type_
    ]
  , cmdCallback  = cb
  }

monitor ::
     T.Text
  -> CallbackHandler
  -> Command
monitor logLevel cb = Command
  { cmdName      = "monitor"
  , cmdWantsBody = True
  , cmdOneShot   = False
  , cmdBody      = Just $ Frame
    [ "LogLevel" .= logLevel
    ]
  , cmdCallback  = cb
  }

stop ::
     Int
  -> CallbackHandler
  -> Command
stop s cb = Command
  { cmdName      = "stop"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Stop" .= s
    ]
  , cmdCallback  = cb
  }

leave ::
     CallbackHandler
  -> Command
leave cb = Command
  { cmdName      = "leave"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Nothing
  , cmdCallback  = cb
  }

query ::
     [T.Text]
  -> HM.HashMap T.Text T.Text
  -> Bool
  -> Int
  -> T.Text
  -> BS.ByteString
  -> CallbackHandler
  -> Command
query ns ts ack timeout name payload cb = Command
  { cmdName      = "query"
  , cmdWantsBody = True
  , cmdOneShot   = False
  , cmdBody      = Just $ Frame
    [ "FilterNodes" .= ns
    , "FilterTags"  .= ts
    , "RequestAck"  .= ack
    , "Timeout"     .= timeout
    , "Name"        .= name
    , "Payload"     .= payload
    ]
  , cmdCallback  = cb
  }

respond ::
     Int
  -> BS.ByteString
  -> CallbackHandler
  -> Command
respond id_ payload cb = Command
  { cmdName      = "respond"
  , cmdWantsBody = False
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "ID"      .= id_
    , "Payload" .= payload
    ]
  , cmdCallback  = cb
  }

installKey ::
     T.Text
  -> CallbackHandler
  -> Command
installKey key cb = Command
  { cmdName      = "install-key"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Key" .= key
    ]
  , cmdCallback  = cb
  }

useKey ::
     T.Text
  -> CallbackHandler
  -> Command
useKey key cb = Command
  { cmdName      = "use-key"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Key" .= key
    ]
  , cmdCallback  = cb
  }

removeKey ::
     T.Text
  -> CallbackHandler
  -> Command
removeKey key cb = Command
  { cmdName      = "remove-key"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Key" .= key
    ]
  , cmdCallback  = cb
  }

listKeys ::
     CallbackHandler
  -> Command
listKeys cb = Command
  { cmdName      = "list-keys"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Nothing
  , cmdCallback  = cb
  }

stats ::
     CallbackHandler
  -> Command
stats cb = Command
  { cmdName      = "stats"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Nothing
  , cmdCallback  = cb
  }

getCoordinate ::
     T.Text
  -> CallbackHandler
  -> Command
getCoordinate node cb = Command
  { cmdName      = "get-coordinate"
  , cmdWantsBody = True
  , cmdOneShot   = True
  , cmdBody      = Just $ Frame
    [ "Node" .= node
    ]
  , cmdCallback  = cb
  }

(.=) :: MP.MessagePack a => T.Text -> a -> (T.Text, MP.Object)
(.=) k v = (k, MP.toObject v)
