module Control.Distributed.Process.Serf.Agent
  ( SerfSettings(..), defaultSerfSettings
  , SerfAgent(..), serfAgent
  , command
  ) where

import Prelude

import           Control.Concurrent.STM (TQueue, atomically, newTQueueIO,
                                         readTQueue, writeTQueue)
import           Control.Monad
import           Control.Monad.Catch    (finally)
import           Control.Monad.Trans    (MonadIO)
import qualified Data.ByteString.Char8  as BS
import qualified Data.ByteString.Lazy   as BSL
import qualified Data.HashMap.Strict    as HM
import qualified Data.IntMap            as IM
import qualified Data.MessagePack       as MP
import           Data.Monoid
import qualified Data.Text              as T

import           Control.Distributed.Process hiding (finally)
import           Data.Binary                 (get)
import           Data.Binary.Get             (Decoder (..), runGetIncremental)
import qualified System.Socket               as S

import Control.Distributed.Process.Serf.Types

data SerfSettings = SerfSettings
  { serfHost  :: !String
  , serfPort  :: !Int
  , serfDebug :: !Bool
  } deriving (Show)

defaultSerfSettings :: SerfSettings
defaultSerfSettings = SerfSettings "127.0.0.1" 7373 False

type Socket = S.Socket S.Inet6 S.Stream S.TCP

type Callbacks = IM.IntMap Callback

data Callback = Callback
  { wantsBody :: !Bool
  , oneShot   :: !Bool
  , callback  :: !CallbackHandler
  }

data AgentState = AgentState
  { agentSocket   :: !Socket
  , agentSeq      :: !Int
  , agentReadFrom :: !(ReceivePort Frame)
  , agentSubs     :: !Callbacks
  , agentQueue    :: !(TQueue Command)
  }

data SerfAgent = SerfAgent
  { agentPid      :: !ProcessId
  , agentCommands :: !(TQueue Command)
  }

serfAgent :: SerfSettings -> Process SerfAgent
serfAgent settings = do
  socket <- liftIO connect
  (fromAgentS, fromAgentR) <- newChan
  socketReader fromAgentS socket
  commands <- liftIO newTQueueIO
  pid <- spawnLocal $ do
    let initial = AgentState
          { agentSocket   = socket
          , agentSeq      = 0
          , agentReadFrom = fromAgentR
          , agentSubs     = mempty
          , agentQueue    = commands
          }
    go initial `finally` liftIO (S.close socket)
  pure SerfAgent
    { agentPid = pid
    , agentCommands = commands
    }
  where
    debug :: String -> Process ()
    debug = when (serfDebug settings) . say

    connect :: IO Socket
    connect = do
      let host  = Just $ BS.pack $ serfHost settings
          port  = Just . BS.pack . show $ serfPort settings
          flags = S.aiAll <> S.aiV4Mapped
      S.getAddressInfo host port flags >>= \case
        (info :: S.AddressInfo S.Inet6 S.Stream S.TCP) : _ -> do
          sock <- S.socket
          S.connect sock (S.socketAddress info)
          pure sock
        _ ->
          -- Shouldn't happen™
          fail "AddressInfoException-ish"

    go :: AgentState -> Process ()
    go state@AgentState{..} = say "go" >> receiveWait
      [ matchChan agentReadFrom handleIncoming
      , matchSTM (readTQueue agentQueue) handleCommand
      , matchAny handleTrash
      ]
      where
        handleIncoming frame@(Frame msg) = do
          debug $ "Incoming frame: " <> show msg
          case headerFrame frame of
            Nothing ->
              die ("Unexpected non-header frame" :: T.Text)
            Just (ReplyHeader s e) ->
              case IM.lookup s agentSubs of
                Nothing ->
                  die ("Unexpected seq number" :: T.Text)
                Just (Callback {..}) -> do
                  debug $ "Got reply for " <> show s <> " (" <> show e <> ")"
                  body <- if wantsBody
                    then fmap Just (receiveChan agentReadFrom)
                    else pure Nothing
                  callback (Right body)
                  go state
                    { agentSubs = if oneShot
                        then IM.delete s agentSubs
                        else agentSubs
                    }

        handleCommand Command{..} = do
          debug $ "Incoming command: " <> T.unpack cmdName

          let sub = Callback
                { wantsBody = cmdWantsBody
                , oneShot = cmdOneShot
                , callback = cmdCallback
                }
          let newSubs = IM.insert agentSeq sub agentSubs

          sendRaw state . MP.toObject $ HM.fromList
            [ (T.pack "Command", MP.ObjectStr cmdName)
            , (T.pack "Seq", MP.ObjectInt agentSeq)
            ]
          case cmdBody of
            Nothing ->
              pure ()
            Just (Frame body) ->
              sendRaw state (MP.toObject body)

          go state
            { agentSeq = succ agentSeq
            , agentSubs = newSubs
            }

        handleTrash msg = do
          debug $ "Incoming message: " <> show msg
          go state

    sendRaw :: AgentState -> MP.Object -> Process ()
    sendRaw state msg = void . liftIO $ S.send
      (agentSocket state)
      (BSL.toStrict $ MP.pack msg)
      mempty

    socketReader :: SendPort Frame -> Socket -> Process ()
    socketReader chan sock = do
      parent <- getSelfPid
      link =<< spawnLocal
        (link parent >> step "" (Done "" 0 Nothing))

      where
        getter = fmap (Just . MP.ObjectMap ) (MP.getMap get get)

        step _ (Fail _ _ err) =
          fail ("MsgPack: " <> err)

        step "" (Done leftovers _ mObj) = do
          case fmap MP.fromObject mObj of
            Just (Just msg) ->
              unsafeSendChan chan (Frame msg)
            _ -> pure ()
          step leftovers (runGetIncremental getter)

        step _ (Done _ _ _) =
          die ("MsgPack: unspent leftovers collide" :: T.Text)

        step "" (Partial cont) = do
          debug $ "Requesting data..."
          bs <- liftIO (S.receive sock 2048 mempty)
          debug $ "Got some data: " <> show bs
          if BS.null bs
            then die ("MsgPack: end of stream" :: T.Text)
            else step "" (cont $ Just bs)

        step leftovers (Partial cont) =
          step "" (cont $ Just leftovers)

command :: MonadIO m => SerfAgent -> Command -> m ()
command serf = liftIO . atomically . writeTQueue (agentCommands serf)
